'''  @file                lab_1_description.py
   @brief               Lab1 program functionality 

   @mainpage

   @section sec_intro   The assignment for lab 1 requires the student to familiarize themselves with micropython libraries,
                        the stm32 Nucleo, and hardware interfacing technique.  The student is required to control the blinking
                        of the onboard LED by using the onboard blue button as a harware interrupt.  LED blinking patterns are
                        defined in the ME 305 lab manual

   @section sec_video   Project Video Demonstration
                        Follow this link to see the video demonstration:
                        https://youtu.be/h5kLFTznpdA
                        
   @author              Jason Davis

   @copyright           Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more

   @date                October 4, 2021
'''