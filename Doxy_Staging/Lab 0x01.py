'''   @file                               main.py
   @brief                              A simple program to cycle LED patterns with a button
   @details                            A simple program designed to familiarize the user with micropython, timers, buttons, and LEDs
   @author                             Jason Davis
   @copyright                          Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more
   @date                               October 4, 2021
'''

import pyb, math, time, micropython

#allocates space in the program to display information in the event of an interrupt exception
micropython.alloc_emergency_exception_buf(100)

#initializeing inputs / outputs
button = pyb.Pin (pyb.Pin.cpu.C13)
LED = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP) # onboard LED @ pin A5

tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin = LED)

# global variables
state = int(0)
displayMessage = bool(True)
t1 = time.ticks_ms()
t2 = time.ticks_ms()
dt = float(time.ticks_diff(t2,t1))/1000 # time in seconds

def buttonPressed(IRQ_src):
    '''@brief                              Button interrupt function
       @details                            Unconditionally interrupts program to increment the "state" variable and toggle the "displayMessage" variable
       @param IRQ_src                      Defines the source of the hardware interrupt request
    '''
    global state, displayMessage
    state += 1
    displayMessage = True
    
buttonInt = pyb.ExtInt(button, mode=pyb.ExtInt.IRQ_FALLING, 
                       pull=pyb.Pin.PULL_NONE, callback=buttonPressed)

# called each time a new wave cycle is created
def resetTimer():
    '''@brief                              Resets timekeeping variables
       @details                            Resets timekeeping variables "t1" and "t2". Recalculates the difference in time between them
    '''
    global t1, t2, dt
    t1 = time.ticks_ms()
    t2 = time.ticks_ms()
    dt = abs(time.ticks_diff(t2, t1))/1000

# called during each wave cycle to record time passing    
def updateTimer():
    '''@brief                              Updates timekeeping variables
       @details                            Updates timekeeping variable "t2". Recalculates the difference in time between "t1" and "t2"
                                           to record the running time.
    '''
    global t2, dt
    t2 = time.ticks_ms()
    dt = abs(time.ticks_diff(t2, t1))/1000
    
def squareWave(period):
    '''@brief                              Blinks the LED
       @details                            Blinks the LED at a rate corresponding to the on/off period defined in the main program loop
       @param period                       The period defined in the main program loop that corresponds to the time required to complete one on/off cycle
       @return                             Returns a one or zero depending on the value running timer 
    '''
    value = 0
    if (dt >= 0 and dt < period/2):
        value = 1
    return value

def sineWave(period, phi0):
    '''@brief                              Cyclically fades the LED on and off
       @details                            Cyclically fades the LED on and off corresponding the sine function
       @param period                       The period defined in the main program loop that corresponds to the time required to complete one on/off cycle
       @param phi0                         The value that the sine function is shifted left or right by. Also known as the initial phase constant in simple
                                           harmonic motion
       @return                             Returns a one or zero depending on the value running timer 
    '''
    omega = (2 * math.pi)/period
    phase = (omega * dt) + phi0
    return math.sin(phase)

if __name__ == '__main__':
#main loop runs indefinitely unless interrupted
    while (True):
        try:
            
            #square wave done
            if (state == 1):
                
                if (displayMessage):
                    # print ('state')
                    # print (state)
                    print('Square Wave Selected')
                    displayMessage = False
                    resetTimer()
                    updateTimer()
                
                if (dt > 1):    
                    resetTimer()
                    updateTimer()
                t2ch1.pulse_width_percent(abs(squareWave(1) * 100))
                updateTimer()
                    
            #sine wave done
            elif (state == 2):
                
                if (displayMessage):
                    # print ('state')
                    # print (state)
                    print('Sine Wave Selected')
                    displayMessage = False
                    resetTimer()
                    updateTimer()
                    
                t2ch1.pulse_width_percent(abs(sineWave(10, (math.pi)/6) * 100))
                updateTimer()
                    
            #sawtooth wave done
            elif (state == 3):
               
                if (displayMessage):
                    # print ('state')
                    # print (state)
                    print('Sawtooth Wave Selected')
                    displayMessage = False
                    resetTimer()
                    updateTimer()

                if (dt > 1):
                    resetTimer()
                t2ch1.pulse_width_percent(abs(dt * 100))
                updateTimer()  
                
            elif (state == 0):
                if (displayMessage):
                    # print ('state')
                    # print (state)
                    print ('Press the blue button to cycle wave patterns')
                    displayMessage = False
            
            else:
                state = 1
        except KeyboardInterrupt:
            break
    print('Program Terminating. Have a nice day!')