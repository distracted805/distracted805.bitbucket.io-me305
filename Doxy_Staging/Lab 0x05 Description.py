'''   @file                        lab_5_description.py
   @brief                       Lab 5 Functionality
   
   @mainpage                    
   
   @section sec_intro           The assignment for lab 5 requires the student to interface with the BNO055
                                orientation sensor.  In this lab, a driver for the IMU was written such that
                                the sensor can be calibrated and then used to read Euler angles from the IMU.
                                These angles can then be used as state measurements as part of a larger program.
                                Additionally, angular velocity can also be read from the IMU and used as state
                                measurements. 
                                
    @section sec_video          Project Video Demonstration
                                Follow this link to see the video demonstration:
                                https://youtu.be/d4gY1B1i5i4
                                
    @author                     Conor Fraser
    @author                     Jason Davis
    
    @copyright                  Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more
    
    @date                       November 6, 2021
'''