''' @file                       main.py
    @brief                      The main function to call a driver for the 9-axis BNO055 IMU
    @details
    @author                     Jason Davis
    @author                     Conor Fraser
    @date                       October 11, 2021
'''

from pyb import I2C
import BNO055
import utime




# action items
# sleep function wrong?

if __name__ == '__main__':  
    print('Program Start')
    
    i2c = I2C(1, I2C.MASTER)                    # intialize i2c object 
    i2c.init(I2C.MASTER, baudrate = 500000)     # calls the init function
    
    utime.sleep(.5)     # time delay to let I2C port to activate
    
    IMUDriver = BNO055.BNO055(0x28, i2c)
    
    calibration = False
    
    while (calibration == False):
        try:
            status = IMUDriver.get_calibration_status()
            print("Calibration Value: " + str(status))
            #print('test calibration loop runs')
            utime.sleep(.25)
            
            if(status[0] + status[1] + status[2] + status[3] == 12):     # checking to see if calibration is complete by confirmng all four 
                calibration = True
                print('Calibration completed \n'
                      'Calibration Status: '+str(IMUDriver.get_calibration_coefficient()))
                while (True):
                    try:
                        print('Euler: [Heading, Roll, Pitch] '+str(IMUDriver.read_euler_angles()))
                        print('Angular Velocity: '+str(IMUDriver.read_angular_velocity()))
                        print('\n')
                        utime.sleep(.25)
                              
                    except KeyboardInterrupt:
                        break
        except KeyboardInterrupt:
            break
    print('End of Program')