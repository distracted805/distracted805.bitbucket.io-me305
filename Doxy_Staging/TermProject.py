'''
@page "Term Project"

@section sec_title Term Project: Ball Balancing Platform

@author Zachary Stednitz
@author Jason Davis
@author Conor Fraser
@author Solie Grantham

@date 12/8/2021

@section sec_overview Overview
The purpose of this lab was to combine all of the task based programs we made 
throughout the quarter and use them to balance a ball on top of a moveable platform.
This is achieved with a combination of data gathered from the IMU and also positional
data gathered from ADC readings of the resistive touch panel. After we get this data
we perform our mathematical analysis on it, which can be seen in Homework2and3.py \ref
Once we perform this analysis, we can use it to set the duty cycle of each motor
independently in order to attempt to balance the ball. 

To see the code implemenation, refer to this repository: 
    https://bitbucket.org/zstednit/me305_labs/src/master/TermProject/Final%20Term%20Project/
    
To see documentation for this file, visit
    BNO055.py \ref
    closed_loop_controller.py \ref
    DRV8847.py \ref
    main.py \ref
    panelDriver.py \ref
    task_closed_loop_controller.py \ref
    task_IMU.py \ref
    task_motor.py \ref
    task_motorDriver.py \ref
    task_panel.py \ref
    task_user.py \ref
    vector.py \ref
'''