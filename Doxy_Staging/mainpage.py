'''   @file                        mainpage.py
   @brief                       ME305 Fall 2021 Project List
   
   @mainpage                    
   
   @tableofcontents 		       

   @section sec_lab0x00 	    Lab 0x00: Fibonacci Calculator
                                A simple function to retrieve the "nth" fibonacci number   
   
   @author                      Jason Davis
   @date 			            September 27, 2021
   @copyright                   Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more
   
   @section sec_lab0x01 		Lab 0x01: LED
                                The assignment for lab 1 requires the student to familiarize themselves with micropython libraries,
                                the stm32 Nucleo, and hardware interfacing technique.  The student is required to control the blinking
                                of the onboard LED by using the onboard blue button as a harware interrupt.  LED blinking patterns are
                                defined in the ME 305 lab manual.

                                Project Video Demonstration:
                                
   @link video_Demo		        https://youtu.be/h5kLFTznpdA 
   @endlink    
   
   @author              	    Jason Davis
   @date                	    October 8, 2021
   @copyright                   Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more
          
   @section sec_lab0x02 		Lab 0x02: Incremental Encoders
                                This lab introduces the student to the quadrature encoder and requires them to write a hardware driver to 
                                interface with the encoder.  This driver is able to retrieve the position and angular velocity of the attached
                                hardware as well as zero the position and gather data from the encoder.  This assignment also requires the
                                student to write a user interface. The expected outcome is that the program will achieve multitasking rather
                                than halting to perform each task.
                                
   @author              	    Jason Davis
   @date                	    October 21, 2021
   @copyright                   Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more

   @section sec_lab0x03 		Lab 0x03: PMDC Motors
                                Building on the code from the previous lab, this assignment requires the student to drive integrated dc motors
                                via pulse width modulation.  This action is achieved by writing a hardware driver for the DRV8847 motor driver
                                integrated H-bridge circuit.  Students are required to vary the speed of the motors and handle various fault
                                conditions associated with the hardware.
                                
   @author              	    Jason Davis
   @author                      Conor Fraser
   @author                      Solie Grantham
   @author                      Zachary Stednietz
   
   @date                	    November 18, 2021
   @copyright                   Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more
   
   @section sec_lab0x04 		Lab 0x04: Closed Loop Speed Control
                                This assignment achieves closed loop speed control of the dc motors by taking in a target angular velocity and gain
                                and outputting a pulse width modulation value to achieve this target.  Student's controller code builds on previous
                                code base to achieve closed-loop control.
                                
   @author              	    Jason Davis
   @author                      Conor Fraser
   @author                      Solie Grantham
   @author                      Zachary Stednietz
   
   @date                	    November 18, 2021
   @copyright                   Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more
   
   @section sec_lab0x05         Lab 0x05: Inertial Measurement Unit (IMU)
                                The assignment for lab 5 requires the student to interface with the BNO055
                                orientation sensor.  In this lab, a driver for the IMU was written such that
                                the sensor can be calibrated and then used to read Euler angles from the IMU.
                                These angles can then be used as state measurements as part of a larger program.
                                Additionally, angular velocity can also be read from the IMU and used as state
                                measurements. 

                                Project Video Demonstration
                                Follow this link to see the video demonstration:
                                https://youtu.be/d4gY1B1i5i4           
    
   @author                      Conor Fraser                            
   @author                      Jason Davis
   @date                        November 11, 2021
   @copyright                   Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more


   @page TermProjAnalysis       HW 0x02: Ball Balance System Modeling 
                                This assignment reflects the analysis of motion of a ball bearing resting on the touch panel
                                and the associated kinematic equations which describe the system behavior.  These equations relate
                                linear and angular acceleration of the ball/platform system to applied motor torque. The analysis is
                                shown below:
                     
   @author              	    Jason Davis
   @author                      Conor Fraser
   
   @date                	    December 1, 2021
   @copyright                   Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more

   @page TermProjSimulation     HW 0x03: Ball Balance System Simulation 
                                This assignment simulates system behavior given various input parameters. The plots below give a graphical
                                representation of system behavior with respect to time:
                     
   @author              	    Conor Fraser
   @author                      Jason Davis
   
   @date                	    December 1, 2021
   @copyright                   Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more
'''