var annotated_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "closed_loop_controller", null, [
      [ "closedLoopController", "classclosed__loop__controller_1_1closedLoopController.html", "classclosed__loop__controller_1_1closedLoopController" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "gainVector", null, [
      [ "GainVector", "classgainVector_1_1GainVector.html", "classgainVector_1_1GainVector" ]
    ] ],
    [ "panelDriver", null, [
      [ "panelDriver", "classpanelDriver_1_1panelDriver.html", "classpanelDriver_1_1panelDriver" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_closed_loop_controller", null, [
      [ "Task_Closed_Loop_Controller", "classtask__closed__loop__controller_1_1Task__Closed__Loop__Controller.html", "classtask__closed__loop__controller_1_1Task__Closed__Loop__Controller" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_IMU", null, [
      [ "Task_IMU", "classtask__IMU_1_1Task__IMU.html", "classtask__IMU_1_1Task__IMU" ]
    ] ],
    [ "task_motor", null, [
      [ "Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_motorDriver", null, [
      [ "Task_motorDriver", "classtask__motorDriver_1_1Task__motorDriver.html", "classtask__motorDriver_1_1Task__motorDriver" ]
    ] ],
    [ "task_panel", null, [
      [ "Task_Panel", "classtask__panel_1_1Task__Panel.html", "classtask__panel_1_1Task__Panel" ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ],
    [ "vector", null, [
      [ "KinematicVector", "classvector_1_1KinematicVector.html", "classvector_1_1KinematicVector" ]
    ] ]
];