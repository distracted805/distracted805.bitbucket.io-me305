/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 305 Projects", "index.html", [
    [ "Welcome to my ME 305 Portfolio!", "index.html", null ],
    [ "Lab 0x00: Fibonacci Calculator", "lab0x00.html", null ],
    [ "Lab 0x01: LED", "lab0x01.html", null ],
    [ "Lab 0x02: Incremental Encoders", "lab0x02.html", null ],
    [ "Lab 0x03: PMDC Motors", "lab0x03.html", null ],
    [ "Lab 0x04: Closed Loop Speed Control", "lab0x04.html", null ],
    [ "Lab 0x05: Inertial Measurement Unit (IMU)", "lab0x05.html", null ],
    [ "HW 0x00: Finite State Machine", "HW0.html", null ],
    [ "HW 0x02: Ball Balance System Modeling", "HW2.html", null ],
    [ "HW 0x03: Ball Balance System Simulation", "HW3.html", null ],
    [ "Term Project", "TermProject.html", [
      [ "Term Project Overview", "TermProject.html#sec_overview", [
        [ "Objective", "TermProject.html#Purpose", null ],
        [ "Design Considerations", "TermProject.html#Design", null ],
        [ "Finite State Machine", "TermProject.html#fsm", null ],
        [ "Closed Loop Control", "TermProject.html#Control", null ],
        [ "Spatial Orientation", "TermProject.html#imu", null ],
        [ "Touch Panel", "TermProject.html#panel", null ]
      ] ],
      [ "Project Demonstration", "TermProject.html#Demonstration", null ],
      [ "Problems, Oversights, and Other \"Learning Opportunities\"", "TermProject.html#issues", [
        [ "Explanation of Missing Deliverables, Controller Malfunctioning", "TermProject.html#submission", null ],
        [ "The Good", "TermProject.html#good", null ],
        [ "The Bad", "TermProject.html#bad", null ],
        [ "The Ugly", "TermProject.html#ugly", null ]
      ] ],
      [ "Credits", "TermProject.html#credits", [
        [ "Source Code", "TermProject.html#src", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"BNO055_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';