var searchData=
[
  ['main_2epy_0',['main.py',['../main_8py.html',1,'']]],
  ['mainpage_2epy_1',['mainpage.py',['../mainpage_8py.html',1,'']]],
  ['modifygainmatrix_2',['modifyGainMatrix',['../classclosed__loop__controller_1_1closedLoopController.html#a7769048c6cac1c8216c86be3483d065c',1,'closed_loop_controller::closedLoopController']]],
  ['modifymotoroperation_3',['modifyMotorOperation',['../classtask__motor_1_1Task__Motor.html#a21ed487ff8f96ff91a15d3d38c84a10b',1,'task_motor::Task_Motor']]],
  ['modifyvectorcomponent_4',['modifyVectorComponent',['../classgainVector_1_1GainVector.html#ab770ab829bbfba984fe580db396928dc',1,'gainVector::GainVector']]],
  ['motor_5',['Motor',['../classDRV8847_1_1Motor.html',1,'DRV8847']]],
  ['motor_6',['motor',['../classDRV8847_1_1DRV8847.html#aa82b0ffa53e9f6bb351691f85f71699c',1,'DRV8847::DRV8847']]],
  ['motor_5fshare_7',['motor_share',['../classtask__user_1_1Task__User.html#a67ea153bbb519c7a1d3a49514a510f2d',1,'task_user::Task_User']]]
];
