# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 15:27:55 2021

@author: jason
""" 
#validates the user input prior to calculating fibonacci number
def validateInput(idx):
    
    flag = False
    if idx.isnumeric() and int(idx) >= 0:
        flag = True
   
    return flag

#fibonacci calculation is done here
#input validation called by this function
def fib(idx):
    
    result = int(-1)
    fibList = [0,1]
   
    if int(idx) < len(fibList):
        result = idx
    else:
        while len(fibList) <= int(idx): # Not necessary to explicitly define these variables,
            num1 = int(fibList[-1])     # but it helps me understand the python syntax better
            num2 = int(fibList[-2])
            result = num1 + num2
            fibList.append(result)
    return result

if __name__ == '__main__':

    flag = 'y' # can be anything except x
    idx = input('Please enter a positive integer: ')
    
    while flag != 'x':
        
        while (not validateInput(idx)):
            print ('Invalid user input!')
            idx = input ('Please enter a positive integer: ')
            validateInput(idx)

        print ('Fibonacci number at index {:} is {:}.'.format(idx,fib(idx)))
        flag = input('Please enter a positve integer or enter "x" to exit: ')
        idx = flag
        
    print('Good bye!')