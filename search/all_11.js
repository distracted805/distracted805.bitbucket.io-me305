var searchData=
[
  ['s0_5finit_0',['S0_init',['../task__encoder_8py.html#a118da909b013744f2b2ef4febcca4e32',1,'task_encoder.S0_init()'],['../task__motor_8py.html#aaa32437a8951515fe5ce41df8fe1e298',1,'task_motor.S0_init()'],['../task__user_8py.html#a09d12fac9d0696e1f015737e65327368',1,'task_user.S0_init()']]],
  ['savegainmatrices_1',['saveGainMatrices',['../classclosed__loop__controller_1_1closedLoopController.html#ab9423289599fa44125c306f41df25472',1,'closed_loop_controller::closedLoopController']]],
  ['scanx_2',['scanX',['../classpanelDriver_1_1panelDriver.html#ad8b7261a6e00de1dcc822fefa5b49529',1,'panelDriver::panelDriver']]],
  ['scany_3',['scanY',['../classpanelDriver_1_1panelDriver.html#a25b7a6b04e0e0fe3897239c144c3ef7e',1,'panelDriver::panelDriver']]],
  ['ser_4',['ser',['../classtask__encoder_1_1Task__Encoder.html#a84b0e80344084871098b90dfd111fc93',1,'task_encoder.Task_Encoder.ser()'],['../classtask__user_1_1Task__User.html#ae1e7bc8b7b912e51605e649affd85438',1,'task_user.Task_User.ser()']]],
  ['set_5fposition_5',['set_position',['../classencoder_1_1Encoder.html#a097746ac59abf28e6567f5604fe83c1f',1,'encoder::Encoder']]],
  ['setcalibrationstatus_6',['setCalibrationStatus',['../classBNO055_1_1BNO055.html#a835df25edaea373d143a9db150b8289d',1,'BNO055::BNO055']]],
  ['setduty_7',['setDuty',['../classDRV8847_1_1Motor.html#a3a4462b2716495a75b3dca4b02417ec4',1,'DRV8847::Motor']]],
  ['seterromegax_8',['setErrOmegaX',['../classvector_1_1KinematicVector.html#a3cfa365fc85d5fc487cc5d43b10ebe3c',1,'vector::KinematicVector']]],
  ['seterromegay_9',['setErrOmegaY',['../classvector_1_1KinematicVector.html#a2a8fccfb8b04464e7f188c1a8c62fe70',1,'vector::KinematicVector']]],
  ['seterrthetax_10',['setErrThetaX',['../classvector_1_1KinematicVector.html#a8335705866478a5b73b702755bdbc414',1,'vector::KinematicVector']]],
  ['seterrthetay_11',['setErrThetaY',['../classvector_1_1KinematicVector.html#abe9ecb0baca1bbb1da6421d107b77dd3',1,'vector::KinematicVector']]],
  ['setomegax_12',['setOmegaX',['../classvector_1_1KinematicVector.html#adee05b0460f3ce1965b982282f40d5c7',1,'vector::KinematicVector']]],
  ['setomegay_13',['setOmegaY',['../classvector_1_1KinematicVector.html#acf1308b3c6c3f72ce67aa088b0389fbc',1,'vector::KinematicVector']]],
  ['setthetax_14',['setThetaX',['../classvector_1_1KinematicVector.html#a6efec9139ffc8bb1a7ce59996725f0be',1,'vector::KinematicVector']]],
  ['setthetay_15',['setThetaY',['../classvector_1_1KinematicVector.html#a9363402a89d6308e8fe14778a4fe60b9',1,'vector::KinematicVector']]],
  ['setx_16',['setX',['../classvector_1_1KinematicVector.html#a7527175ac03c9abc0965f136ccf6038a',1,'vector::KinematicVector']]],
  ['setxdot_17',['setXDot',['../classvector_1_1KinematicVector.html#ad1b3a2a03a480a1015a5346f335ef10e',1,'vector::KinematicVector']]],
  ['sety_18',['setY',['../classvector_1_1KinematicVector.html#a26b2822d109eac65bcd9935f6db71d90',1,'vector::KinematicVector']]],
  ['setydot_19',['setYDot',['../classvector_1_1KinematicVector.html#a7313e81719ad1daf86e21ca8bc9096ed',1,'vector::KinematicVector']]],
  ['share_20',['Share',['../classshares_1_1Share.html',1,'shares']]],
  ['shares_2epy_21',['shares.py',['../shares_8py.html',1,'']]],
  ['start_5ftime_22',['start_time',['../classtask__user_1_1Task__User.html#a3d9d060205ad72927810d986dc75f493',1,'task_user::Task_User']]],
  ['state_23',['state',['../classtask__encoder_1_1Task__Encoder.html#af038aa706137ba698bf272c011091e50',1,'task_encoder.Task_Encoder.state()'],['../classtask__IMU_1_1Task__IMU.html#a781da8ac148b265b8760646f9730c523',1,'task_IMU.Task_IMU.state()'],['../classtask__user_1_1Task__User.html#afefb79be360ac39f0ed9920de91f953e',1,'task_user.Task_User.state()']]]
];
