'''
@file:      main.py
@brief      Main program for running the motor, IMU, and panel tasks
@details    Creates IMU, panel, and motor objects, the shares for 
            sharing tasks between programs, and creating the tasks
            for each object. Runs the tasks when the user inputs them on the
            user interface
        
@author:    Jason Davis
@author:    Conor Fraser
@author:    Solie Grantham
@author:    Zachary Stednitz
@date:      December 8th, 2021
'''

import pyb, utime, shares, vector
import DRV8847, closed_loop_controller, panelDriver, BNO055
import task_user, task_motor, task_motorDriver
import task_closed_loop_controller, task_IMU, task_panel
from pyb import I2C
    
print('Executing system setup functions...\n')

print('Initializing system components...')  
#creating motor driver / motor objects
print('    DRV8847...', end = '')
motorDriver = DRV8847.DRV8847(pyb.Timer(3, freq=20000))
print('done')

print('    Motors...', end = '')
input1 = pyb.Pin.cpu.B4
input2 = pyb.Pin.cpu.B5
input3 = pyb.Pin.cpu.B0
input4 = pyb.Pin.cpu.B1
motor_A = motorDriver.motor(input1, input2, 1, 2, "MOTOR A")
motor_B = motorDriver.motor(input3, input4, 3, 4, "MOTOR B")
print('done')

print('    Closed loop controller...', end = '')
# controller = closed_loop_controller.closedLoopController('m')
controller = closed_loop_controller.closedLoopController(None)
print('done')

print('    BNO055...', end = '')
print('establishing I2C communication...', end = '')
i2c = I2C(1, I2C.MASTER)                    # intialize i2c object 
i2c.init(I2C.MASTER, baudrate = 500000)     # calls the init function
# time delay to let I2C port to activate
utime.sleep(.5)     
imu = BNO055.BNO055(0x28, i2c, False)
print('done')

print('    Touch panel...', end = '')
Xp = pyb.Pin.board.PA7
Xm = pyb.Pin.board.PA1
Yp = pyb.Pin.board.PA6
Ym = pyb.Pin.board.PA0
xDim = 176
yDim = 100
inputPins = [Xp, Xm, Yp, Ym]    
panelDims = [xDim, yDim]
touchPanel = panelDriver.panelDriver(inputPins, panelDims)
print('done')

devices = [motorDriver, motor_A, motor_B, controller, imu, touchPanel]
print('System components successfully initialized!\n')

print('Initializing shares...', end = '')
#creating relevant share objects for the motors
motor_share = shares.Share()
output_share = shares.Share(0)
delta_share = shares.Share(0)
controller_share = shares.Share()
imu_share = shares.Share(0)
panel_share = shares.Share()

#v is added to the shares list bc it functions much the same as the share
kinVector = vector.KinematicVector()
shareList = [motor_share, delta_share, output_share, controller_share, imu_share, panel_share]
print('done')

print('Building user interface...', end = '')
ui = task_user.Task_User('USER', 40000, shareList, kinVector, dbg=False)
print('done')

print('Initializing task objects...', end = '')
task_motorDriver = task_motorDriver.Task_motorDriver('MOTOR DRIVER', motorDriver, [motor_A, motor_B] , motor_share, 10000, False)
task_motorA = task_motor.Task_Motor('MOTOR_A', 10000, motor_A, motor_share, output_share)
task_motorB = task_motor.Task_Motor('MOTOR_B', 10000, motor_B, motor_share, output_share)
task_controller = task_closed_loop_controller.Task_Closed_Loop_Controller('CONTROLLER', 4000, devices, shareList, kinVector, False)
task_imu = task_IMU.Task_IMU('IMU', imu, 1000, shareList, kinVector)
task_panel = task_panel.Task_Panel('PANEL', touchPanel, 1000, panel_share, kinVector)

taskList = [ui, task_motorDriver, task_motorA, task_motorB, task_controller, task_imu, task_panel]
print('done\n')

print('System Ready\n')
systemCalibrated = task_imu.getIMU().getCalibrationStatus()

# input('Press [enter] to begin IMU calibration: ')
print('Calibrating IMU...')
while (not systemCalibrated):
    task_imu.calibrate()
    systemCalibrated = task_imu.getIMU().getCalibrationStatus()
task_imu.zeroPanelOrientation(False)
print('IMU calibration complete') 
   
while (True):
    try:
        for task in taskList:
            task.run()
    except KeyboardInterrupt:
        break
print('\n*** Program ending, have a nice day! ***\n')