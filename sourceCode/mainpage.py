''' @file                       mainpage.py
    @brief                      ME305 Fall 2021 Project List

    @mainpage                   Welcome to my ME 305 Portfolio! 
    
    @subsection intro           Introduction
                                This website is a repository for all of the software projects that were completed as part of the
                                ME 305: Introduction to Mechatronics course.\n  The menu to the left provides links to all of the projects with
                                video demonstrations and source code available for viewing.  
                                
    @image realistic            <img src="realisticModel.jpg">   
             
    @page lab0x00        	    Lab 0x00: Fibonacci Calculator
    
    @subsection description     Description
                                A simple function to retrieve the "nth" fibonacci number in the sequence   

    @author                     Jason Davis
    @date 			            September 27, 2021
    @copyright                  Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more
   
    @page lab0x01            	Lab 0x01: LED
                                
    @subsection description     Description
                                The assignment for lab 1 requires the student to familiarize themselves with micropython libraries,
                                the stm32 Nucleo, and hardware interfacing technique.  The student is required to control the blinking
                                of the onboard LED by using the onboard blue button as a harware interrupt.  LED blinking patterns are
                                defined in the ME 305 lab manual. Click <<A HREF="https://youtu.be/h5kLFTznpdA">Lab0x01 Video</A>> to 
                                view the project demo.
  
    @author              	    Jason Davis
    @date                	    October 8, 2021
    @copyright                  Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more
          
    @page lab0x02        		Lab 0x02: Incremental Encoders
     
    @subsection description     Description
                                This lab introduces the student to the quadrature encoder and requires them to write a hardware driver to 
                                interface with the encoder.  This driver is able to retrieve the position and angular velocity of the attached
                                hardware as well as zero the position and gather data from the encoder.  This assignment also requires the
                                student to write a user interface. The expected outcome is that the program will achieve multitasking rather
                                than halting to perform each task.
                                    
    @author              	    Jason Davis
    @date                	    October 21, 2021
    @copyright                  Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more

    @page lab0x03        		Lab 0x03: PMDC Motors
    
    @subsection description     Description
                                Building on the code from the previous lab, this assignment requires the student to drive integrated dc motors
                                via pulse width modulation.  This action is achieved by writing a hardware driver for the DRV8847 motor driver
                                integrated H-bridge circuit.  Students are required to vary the speed of the motors and handle various fault
                                conditions associated with the hardware. Click <<A HREF="https://youtu.be/Rxygu1pWYa4">Lab0x03 Video</A>> to view project demo.
    
    @subsection plots           Encoder Plots
    
    @image p1                   <img src="lab3_1_pvt.jpg" width="600" height="375">   <img src="lab3_1_dvt.jpg" width="600" height="375">

    @image p2                   <img src="lab3_2_pvt.jpg" width="600" height="375">   <img src="lab3_2_dvt.jpg" width="600" height="375">      
        
    @author              	    Jason Davis
    @author                     Conor Fraser
    @author                     Solie Grantham
    @author                     Zachary Stednietz
       
    @date                	    November 18, 2021
    @copyright                  Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more
       
    @page lab0x04 		        Lab 0x04: Closed Loop Speed Control
     
    @subsection description     Description                            
                                This assignment achieves closed loop speed control of the dc motors by taking in a target angular velocity and gain
                                and outputting a pulse width modulation value to achieve this target.  Student's controller code builds on previous
                                code base to achieve closed-loop control. Click <<A HREF="https://youtu.be/tG4qmKjGVGg">Lab0x04 Video</A>> to view project demo.
    
    @subsection plots           Encoder Plots
    
    @image p1                   <img src="lab4_1.jpg" width="600" height="375">   <img src="lab4_2.jpg" width="600" height="375">

    @image p2                   <img src="lab4_3.jpg" width="600" height="375">   <img src="lab4_4.jpg" width="600" height="375">  
    
    @author              	    Jason Davis
    @author                     Conor Fraser
       
    @date                	    November 18, 2021
    @copyright                  Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more
       
    @page lab0x05               Lab 0x05: Inertial Measurement Unit (IMU)
                                The assignment for lab 5 requires the student to interface with the BNO055
                                orientation sensor.  In this lab, a driver for the IMU was written such that
                                the sensor can be calibrated and then used to read Euler angles from the IMU.
                                These angles can then be used as state measurements as part of a larger program.
                                Additionally, angular velocity can also be read from the IMU and used as state
                                measurements. Click <<A HREF="https://youtu.be/d4gY1B1i5i4">Lab0x05 Video</A>> to view our
                                video demo.                                     
    
    @author                     Conor Fraser                            
    @author                     Jason Davis
    @date                       November 11, 2021
    @copyright                  Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more

    @page HW0                   HW 0x00: Finite State Machine
                                The purpose of this assignment was to create a finite state machine design for an elevator. See the
                                implentation below.  If the casual observer can understand it, it was probably designed well.
                                
    @image hw0fsm               <img src="hw0_fsm.jpeg" width="832" height="1031">                               
                         
    @author              	    Jason Davis
       
    @date                	    September 26, 2021
    @copyright                  Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more
    
    @page HW2                   HW 0x02: Ball Balance System Modeling 
                                
    @image simpleModel          <img src="simpleModel_2.jpg">                            
                                
    @subsection description     Description
                                This assignment reflects the analysis of motion of a ball bearing resting on the touch panel
                                and the associated kinematic equations which describe the\n system behavior.  These equations relate
                                linear and angular acceleration of the ball/platform system to applied motor torque. The analysis is
                                shown below:
                                
    @image p1                   <img src="hw2_1.jpg" width="850" height="1100">
    @image p2                   <img src="hw2_2.jpg" width="850" height="1100">
    @image p3                   <img src="hw2_3.jpg" width="850" height="1100">
    @image p4                   <img src="hw2_4.jpg" width="850" height="1100">
    @image p5                   <img src="hw2_5.jpg" width="850" height="1100">
    @image p6                   <img src="hw2_6.jpg" width="850" height="1100">
    @image p7                   <img src="hw2_7.jpg" width="850" height="1100">
                                                         
    @author              	    Jason Davis
    @author                     Conor Fraser
       
    @date                	    December 1, 2021
    @copyright                  Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more

    @page HW3                   HW 0x03: Ball Balance System Simulation 

    @subsection description     Description
                                This assignment simulates system behavior analyzed in <A HREF="https://distracted805.bitbucket.io/HW2">HW0x02</A> given various input parameters. The plots below give a graphical
                                representation of system behavior with respect to time:
     
    @image hw3                  <img src="hw3.jpg" width="900" height="500">
     
    @author              	    Conor Fraser
    @author                     Jason Davis
       
    @date                	    December 1, 2021
    @copyright                  Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more
       
    @page TermProject           Term Project
    @tableofcontents

    @section sec_overview       Term Project Overview

    @subsection Purpose         Objective
                                The primary objective of this project was to implement a closed loop control system in Python
                                that operated on two motors connected\n via mechanical linkages to a resistive touch panel to balance
                                a steel ball bearing autonomously.
                                
    @image realModel            <img src="realisticModel.jpg">
                                
    @subsection Design          Design Considerations
                                Prior to writing the software for the controlled loop controller, we needed to perform some preliminary
                                analysis on our system to develop\n a theoretical model.  Details of this analysis can be found in
                                <A HREF="https://distracted805.bitbucket.io/HW2">HW0x02</A>. Upon finishing our analysis, we implemented 
                                our design\n in Python as shown by the following task diagram:\n                            
    
    @image task_diagram         <img src="tpTaskDiagram.jpeg" width="750" height="463">                                
  
    @subsection fsm             Finite State Machine
                                Below is a top-level finite state machine design for our project.  We made heavy use of the share.Shares
                                to achieve multitasking\n and run-time optimization
    
    @image fsm                  <img src="tpFSM.jpeg" width="750" height="506">                              

    @subsection Control         Closed Loop Control
                                A closed loop controller was implented to attempt to balance the platform.  This required us to adjust our
                                gain values carefully to avoid overcorrection of the motor inputs.  We immediately found this to be a
                                particularly delicate procedure, therefore functionality was added to our code to help us adjust the gain 
                                values in increments of .00005 using keystrokes while the program ran continuously.  Our objective with this
                                procedure was to be able to titrate our input to a functional set of values quickly and effectively.
                                There were nonetheless problems that surfaced which is outlined in later discussion. The diagram below 
                                demonstrates our design intent with the closed loop controller feedback.\n
                                
    @image block_diagram        \n<img src="tpBlockDiagram.jpeg" width="750" height="228">\n                             
                                
    @subsection imu             Spatial Orientation
                                To achieve balance and closed loop control, our project first required a way to receive input based on its
                                 orientation in space and be able to compare it to a known value (or set of values) that represent "home." 
                                 To achieve this, we used the BNO055 inertial measurement unit based on Lab0x05.  Our approach dictated that
                                 prior to running our program loop, the sensor would self calibrate by reading from a .txt file in the directory.
                                 This data effectively "zeroed" the the sensor's home position to an orientation level with the horizon. To read 
                                 more about the BNO055 sensor please click <A HREF="https://distracted805.bitbucket.io/lab0x05">here</A>. As a 
                                 backup method, we included functionality in our program to manually level the sensor and set the offsets.  This 
                                 function did incorporate "halting" code, however we felt this was justified due to the critical importance of
                                  running the program with a well-calibrated sensor.

    @subsection panel           Touch Panel                           
                                The final component of our project was incorporating the resistive touch panel which would be responsible for 
                                detecting the position and linear velocity of the ball and providing this input to the closed loop controller. Our 
                                design dictated that this kinematic data would be updated upon each iteration of the finite state machine so that 
                                the controller would always have "fresh" data to work with.
    
    @section Demonstration      Project Demonstration
                                To see a video demonstration explaining our thought process and how our code interacted with the user, 
                                Please follow the <A HREF="https://www.youtube.com/watch?v=7xuR2YwOMh8">project demo link to see our video</A>  \n                            
    
    @section issues             Problems, Oversights, and Other "Learning Opportunities"
    
    @subsection submission      Explanation of Missing Deliverables, Controller Malfunctioning
                                There are no plots included with our submission. The reason for this is that our controller never functioned autonomously
                                to the point where collecting positional data on the platform would have done anything more than highlight the profound
                                lack of function graphically.  Furthermore, while our hardware driver for the panel was functioning, our task object for
                                the panel was not.  Consequently, we did not demonstrate required set of features in our video.
                                
    @subsection good            The Good
                                This project provided us with a tremendous learning experience.  The time spent troubleshooting motor faults, mathematical
                                oversights, and control loop anomalies left us with a deep respect for the field of mechatronics and the applications for 
                                which it is well suited.  Additionally, many of the enduring bugs in our code were simple enough to resolve, provided 
                                we could commit the requisite time to refactoring.\n
                                
    @subsection bad             The Bad
                                In the end, our project didn't work.  Not only did it not work, but we kept finding flaws in earlier iterations of the code
                                that likely set us up for failure on the term project.  For example, we discovered in the final week of the quarter that our
                                encoder data had never been properly converted to radians.  It seems as though the root cause of this issue was in our 
                                fundamental misunderstanding of the relationship between cpu clock cycles, timer instantiation, and program runtime.  As previously
                                mentioned, with more time these issues could have been overcome. All it required was more time, which we in short supply of
                                this quarter.
                                
    @subsection ugly            The Ugly
                                After approximately 3000 lines of code, two arthritic hands, and more gray hair than I care to mention, the culmination
                                of our efforts resulted in a malfunctioning robot with what appears to be a severe seizure disorder.                                   
                                
    @section credits            Credits
   
    @author                     Jason Davis
    @author                     Conor Fraser
   
    @date                	    December 9, 2021
    @copyright                  Creative Commons CC BY: Please visit https://creativecommons.org/licenses/by/4.0/ to learn more
    
    @subsection src             Source Code
                                To see documentation for this file, visit \n
                                BNO055.py \ref \n
                                closed_loop_controller.py \ref \n
                                DRV8847.py \ref \n
                                main.py \ref \n
                                panelDriver.py \ref \n
                                task_closed_loop_controller.py \ref \n
                                task_IMU.py \ref \n
                                task_motor.py \ref \n
                                task_motorDriver.py \ref \n
                                task_panel.py \ref \n
                                task_user.py \ref \n
                                vector.py \ref \n
'''