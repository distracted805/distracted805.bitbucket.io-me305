# -*- coding: utf-8 -*-
'''
@file:      panelDriver.py
@brief      Uses ADC readings to locate the position of the ball
@details    When a ball is placed on the platform, the resistive touch panel 
            knows it's there due to the resistive touch panel's circuit network
            We can then use the ADC readings to scan the x y and z direction to
            get an accurate location of the ball. Then to calculate velocity of
            the ball we use an alpha beta filter and equations developed in class.
@author:    Jason Davis
@author:    Conor Fraser
@author:    Solie Grantham
@author:    Zachary Stednitz
@date:      December 8th, 2021
'''
import pyb
from pyb import ADC
from ulab import numpy
import utime

class panelDriver:
    
    def __init__(self, inputPins, panelDims):
        ''' @brief      Initializes a panelDriver object.
            @details    Creates a panel driver object and instantiates the pins.
                        required to read the ADC values.
            @param      inputPins The pins used to read ADC values.
            @param      panelDims A list of panel dimensions.
        '''
        self.Xp = inputPins[0]   # x positive
        self.Xm = inputPins[1]   # x minus 
        self.Yp = inputPins[2]   # y positive
        self.Ym = inputPins[3]   # y minus
        
        self.xLength = panelDims[0]
        self.yLength = panelDims[1]
        
        self.XcoorPoint = 0
        self.YcoorPoint = 0
        
    def getCenter(self):
        ''' @brief      Returns the center position of the platform.
            @details    The x and y dimensions of the panel are brought in from
                        the initializer, and then divided by 2 to get the center location.
        '''
        # computes center of the board from the dimensions of the panel
        # coordinates of the point are xlength/2 and yLength/2
        # returns those coordinates
        xCoor = self.xLength/2
        yCoor = self.yLength/2
        centerCoor = [xCoor, yCoor]     
        
        return centerCoor
            
    def scanX(self):
        ''' @brief      Returns the x coordinate of the ball. 
            @details    Uses the ADC readings from the platform to calculate 
                        the position of x in millimeters.This function also
                        calculates the amount of time it takes to scan this coordinate.
        '''
        
        # origin is the center of the panel
        # returns x coordinate value of the touch location in same units as xLength and yLength
        
        # create some sort of ADC object, or import or something
        
        # set pin output and inputs correctly
        self.Xp.init(pyb.Pin.OUT_PP, value = 1)
        self.Xm.init(pyb.Pin.OUT_PP, value = 0)
        self.Yp.init(pyb.Pin.IN)                  # floating the pins
        self.Ym.init(pyb.Pin.IN)                  # floating the pins 
        utime.sleep_us(4)
        
        timeX = utime.ticks_us()
        ADCx = ADC(self.Ym).read()
        self.XcoorPoint = ADCx * (100/4095) - 50
        deltaX = utime.ticks_diff(utime.ticks_us(), timeX)
        print("Time to scan X: {} microseconds".format(deltaX))
        
        return self.XcoorPoint
    
    def scanY(self):
        ''' @brief      Returns the y coordinate of the ball. 
            @details    Uses the ADC readings from the platform to calculate 
                        the position of y in millimeters.This function also
                        calculates the amount of time it takes to scan this coordinate.
        '''
        # origin is the center of the panel
        # returns y coordinate value of the touch location in same units as xLength and yLength
        
        # set pin output and inputs correctly
        self.Xp.init(pyb.Pin.IN)                     # floating the pins
        self.Xm.init(pyb.Pin.IN)                     # floating the pins
        self.Yp.init(pyb.Pin.OUT_PP, value = 1)
        self.Ym.init(pyb.Pin.OUT_PP, value = 0)
        utime.sleep_us(4)
        
        timeY = utime.ticks_us()
        ADCy = ADC(self.Xm).read()
        self.YcoorPoint = ADCy * (176/4095) - 88
        deltaY = utime.ticks_diff(utime.ticks_us(), timeY)
        print("Time to scan Y: {} microseconds".format(deltaY))
        return self.YcoorPoint
    
    # def ADCx(self):
        
    #     self.Xp.init(pyb.Pin.OUT_PP, value = 1)
    #     self.Xm.init(pyb.Pin.OUT_PP, value = 0)
    #     self.Yp.init(pyb.Pin.IN)                  # floating the pins
    #     self.Ym.init(pyb.Pin.IN)                  # floating the pins 
        
    #     ADCx = ADC(self.Ym).read()
         
    #     return ADCx
    
    # def ADCy(self):
        
    #     self.Xp.init(pyb.Pin.IN)                     # floating the pins
    #     self.Xm.init(pyb.Pin.IN)                     # floating the pins
    #     self.Yp.init(pyb.Pin.OUT_PP, value = 1)
    #     self.Ym.init(pyb.Pin.OUT_PP, value = 0)
        
    #     ADCy = ADC(self.Xm).read()
        
    #     return ADCy
    
    def panelTouch(self):
        ''' @brief      Determines if someting is touching the platform
            @details    Uses the ADC readings from the platform to determine if a
                        ball is on the plate.This function also calculates the 
                        amount of time it takes to scan this coordinate.
        '''
        # returns boolean value whether the board is being forcefully touched or not
        # This is the same as zscan
        self.Xp.init(pyb.Pin.IN)
        self.Xm.init(pyb.Pin.OUT_PP, value = 0)
        self.Yp.init(pyb.Pin.OUT_PP, value = 1)
        self.Ym.init(pyb.Pin.IN)
        utime.sleep_us(4)
        
        timeZ = utime.ticks_us()
        ADCz = ADC(self.Xp).read()
        deltaZ = utime.ticks_diff(utime.ticks_us(), timeZ)
        print("Time to scan Z: {} microseconds".format(deltaZ))
        return ADCz > 20
    
    def readXYZ(self):
        ''' @brief      Returns a tuple of the x y and z values
            @details    Gets the x and y readings from scanX and scanY, and compiles
                        them in a tuple to reduce computation time
        '''
        # returns tuple of x, y, z
        xCoordinate = panelDriverObject.scanX()
        yCoordinate = panelDriverObject.scanY()

        if (panelDriverObject.panelTouch()):
            zCoordinate = 1
        else:
            zCoordinate = 0
        xyzTupleString = (xCoordinate, yCoordinate, zCoordinate)
        xyzTupleInt = tuple(int(num) for num in xyzTupleString.replace('(', '').replace(')', '').replace('...', '').split(', '))
        
        return xyzTupleInt
    
    def getVelocity(self, T_s):
        ''' @brief      Calculates the velocity of the ball using an alpha beta filter
            @details    Using the equations presented in Lecture 24, we can calculate the position
                        and velocity of the ball using readings from the panel and also the 
                        alpha and beta values supplied in lecture. 
            @param      T_s The sample time between alpha beta readings. 
        '''
        x, y, z = self.readXYZ()
        # Create position and velocity variables, but these will be reassigned values
        # later on in the if statement. 
        self.x_initial = 0
        self.y_initial = 0
        self.x_dot_initial = 0
        self.y_dot_initial = 0
        self.alpha = 0.85
        self.beta = 0.005
        self.T_s = T_s  # sample time delta t between readings.
        
        if z == 1: # The ball is on the platform
            x_filter = self.x_final + self.alpha*(x - self.x_final) + self.T_s*self.x_dot_final
            y_filter = self.y_final + self.alpha*(y - self.y_final) + self.T_s*self.y_dot_final
            x_dot_filter = self.x_dot_final + self.beta/self.T_s*(x - self._x_last)
            y_dot_filter = self.y_dot_final + self.beta/self.T_s*(y - self._y_last)
            self.x_final = x_filter
            self.y_final = y_filter
            self.x_dot_final = x_dot_filter
            self.y_dot_final = y_dot_filter
            return (x_filter, x_dot_filter, y_filter, y_dot_filter, z)
        else: # The ball is not in contact with the plate
            return (0, 0, 0, 0, z)
    
    def calibSensorOnPanel(self, uncalPoints):
        ''' @brief      Performs the matrix algrebra needed to estimate the beta matrix value.
            @details    Refer to Lecture 24 for the Matrix Equation:
                        (X^T*X)^-1*X^T*X~
            @param      uncalPoints An array of points on the platform the user taps for calibration
        '''
        # calibrating the panel by locating corners?? that's what I would assume
        # Someone else thought it would only need to be three random points 
        # scattered across the touch panel that would allow calibration
        # Refer to Lecture 24 for the Matrix Equation:
        # (X^T*X)^-1*X^T*X~
        tapPoints = numpy.array([[-80, -40], [80, -40], [-80, 40], [80, 40], [0, 0]])
        calibration_matrix = numpy.dot(numpy.dot(numpy.linalg.inv(numpy.dot(uncalPoints.T, uncalPoints)), uncalPoints.T), tapPoints)
        return calibration_matrix
        
if __name__ == '__main__' :
    
    Xp = pyb.Pin.board.PA7
    Xm = pyb.Pin.board.PA1
    Yp = pyb.Pin.board.PA6
    Ym = pyb.Pin.board.PA0
    xDim = 176
    yDim = 100
    
    inputPins = [Xp, Xm, Yp, Ym]    
    panelDims = [xDim, yDim]
    
    panelDriverObject = panelDriver(inputPins, panelDims)
    
    while (True):
        try:
            if (panelDriverObject.panelTouch()):
                xCoordinate = panelDriverObject.scanX()
                yCoordinate = panelDriverObject.scanY()
                
                print('The panel is being touched: The X coordinate is {:} and the Y coordinate is {:}'.format(xCoordinate, yCoordinate))
                utime.sleep_ms(1000)
            else:
                pass
        
        except KeyboardInterrupt:
            break
    print('\n*** Program ending, have a nice day! ***\n')
         